import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/**That movie database api key for auth3 */
let Key = "3c33469ea417630d76fe29b245a1724b"; 

class App extends Component {
  render() {
    return (

      <div className="App">
        <header className="App-header">
          <div className="Horizontal-container">
            <i className="fas fa-forward fa-3x App-title"></i>
            <h1 className="App-title">Descubre tu proxima pelicula!</h1>
          </div>
          <div className="Search-bar">
            <div className="Horizontal-container">
                <i className="fas fa-search"></i>
                <SearchBar/>
            </div>
          </div>
        </header>
        
        <DiscoverMovieList/>

      </div>
    );
  }
}

/**Search bar element, gets input and return a list of movies with matching titles */
function SearchBar(props){
  return (
  <input type="text" 
         placeholder="Buscar" 
         className="Search-input"></input>
  ); 
}

/**The main component displays the movies grid. contains functionality to show the movie details */
class DiscoverMovieList extends React.Component{
  constructor(props){
    super(props); 

    this.displayMovieDetails = this.displayMovieDetails.bind(this); 
    this.hideMovieDetails = this.hideMovieDetails.bind(this); 

    this.state = {movies: [], lastpage: 0}; 
  }

  componentDidMount(){
    this.fetchMovies(); 
    console.log("loaded movies"); 
  }

  fetchMovies(){
    //TODO move this to an APImanager 
    fetch("https://api.themoviedb.org/3/discover/movie?api_key="+Key)
    .then( response => response.json())
    .then( (data) => { this.updateMovieList(data.results); }); 
  }

  updateMovieList(movies){
    const currentList = this.state.movies; 
    movies.map((movie)=>{  currentList.push(movie) }); 
    this.setState({
      movies : currentList
    }); 
  }

  displayMovieDetails(id){
    fetch("https://api.themoviedb.org/3/movie/"+id+"?api_key="+Key+"&append_to_response=credits")
    .then(response => response.json())
    .then( (data)=>{ 
        this.setState({ 
          movieDetails : data,
          showDetails : true, 
          scrollPosition: window.scrollY
        });

        window.scroll(0,0); 
     }); 
  }

  hideMovieDetails(){
    this.setState({
      showDetails: false
    }); 

    //we return to the last scroll position we were in
    window.setTimeout(()=>{
      window.scrollTo(0, this.state.scrollPosition); 
    }, 500); 
    

  }

  render(){
    return(
      <div>

        {!this.state.showDetails && 
          (
            <div className="Horizontal-container Movie-list">
            {
              this.state.movies.map( (movie, index)=> 
                ( 
                <MovieCard key={index.toString()} movie={movie} clicked={this.displayMovieDetails}/> 
                )
              )
            }
            </div>
          )
        }
       
        {this.state.showDetails && <MovieDetails movie={this.state.movieDetails} GoBack={this.hideMovieDetails}/>}
      </div>  
    ); 
  }
}

/** Card with the movie info */
class MovieCard extends React.Component{
  
  constructor(props){
    super(props); 

    this.state = {
      showDetails : false,
    }

    this.Clicked = this.Clicked.bind(this); 
  }
  
  /** when clicked this element fecths the movie details and displays a MovieDetails View */
  Clicked(){
    this.props.clicked(this.props.movie.id); 
  }
  
  render(){
    return(
      <div className="Movie-card">
        <img src={"http://image.tmdb.org/t/p/w185/"+this.props.movie.poster_path} onClick={this.Clicked} />
        <div className="Details">
          <h4 onClick={this.Clicked} >{this.props.movie.title}</h4>
          <span className="Score">{this.props.movie.vote_average} <i className="fas fa-star"></i> </span>
          <span className="Subtitle"> 
            <span>{ this.props.movie.release_date.split('-')[0] } </span>
          </span>

          <p onClick={this.Clicked} > {this.props.movie.overview.slice(0,120)+"..."} </p>

          <a onClick={this.Clicked}> Mas Informacion </a>
        </div>
      </div>
    )
  }
}


/** Movie details view  */
class MovieDetails extends React.Component{

  constructor(props){
    super(props); 
    console.log(props.movie.credits.cast); 
  }

  render(){
    return(
      <div className="Movie-detail-view">

        <div className="main-detail">
          <div className="Horizontal-container">
            
            <a className="White-btn"
               onClick={this.props.GoBack}
            > <i className="fas fa-arrow-left"></i> Regresar a la lista </a>
            
            <div className=" Flex-container">
                <img src={"http://image.tmdb.org/t/p/w342"+this.props.movie.poster_path} />
                <div className="details">
                    
                    <h2> {this.props.movie.title}  <span>{" ("+this.props.movie.release_date.split('-')[0]+")"}</span> </h2>
                    
                    <div className="Score-details">
                      <span>{(this.props.movie.vote_average*10).toString()+"%"}</span> 
                      <b>User Score</b> 
                    </div>

                    <p>{this.props.movie.overview}</p>

                    <b>Equipo Destacado</b>

                    {this.props.movie.credits.crew.slice(0,3).map( 
                      (crewMenber, index)=> ( 
                        <div className="Crew-member" key={index.toString()}>
                            <b>{crewMenber.name}</b>
                            <br/>
                            <span>{crewMenber.job}</span>
                        </div>
                      ))}
                </div>            
            </div>

          </div>
        </div>

        <div className="Cast">
            
            <div className="Horizontal-container">
              <h3>Elenco Principal</h3>
            </div>
            
            <div className="Flex-container">
                {this.props.movie.credits.cast.slice(0,6).map((castMember, i)=>
                  (
                    <div className="Cast-member" key={i.toString()}>
                      <img src={"http://image.tmdb.org/t/p/w185"+castMember.profile_path}/>
                      <b>{castMember.name}</b>
                      <span>{castMember.character}</span>
                    </div>
                  ))}
            </div> 
        </div>

        <div className="Horizontal-container Other-data">
           <h3>Datos</h3>
           <div className="Flex-container">

                  <Feature title="Titulo Original" value={this.props.movie.original_title}/>                                    

                  <Feature title="Status" value={this.props.movie.status}/>                                    
                  
                  <Feature title="Lenguage Original" value={this.props.movie.original_language}/>                                    

                  <Feature title="Presupuesto" value={this.props.movie.runtime + "M"}/>                  

                  <Feature title="Presupuesto" value={"$"+this.props.movie.budget} />
                  
                  <Feature title="Ingresos" value={"$"+this.props.movie.revenue} />

                  {this.props.movie.homepage && 
                    <div>
                      <b>Web</b> 
                      <a href={this.props.movie.homepage}>{this.props.movie.homepage}</a> 
                    </div>
                  }
           </div>

           <h5>Generos</h5>
           <br/>
           <div className="Flex-container Genres">
                  {this.props.movie.genres.map((genre)=>(
                    <span>{genre.name}</span>
                  ))}
           </div>

           {this.props.movie.video && 
            <h5>Trailer</h5>

           }

        </div>

      </div>
    )
  }
}


class Feature extends React.Component{
  render(){
    return(

      <div className="Feature">
          <b>{this.props.title}</b> 
          <span>{this.props.value}</span> 
      </div>
    
    )
  }
}

export default App;
